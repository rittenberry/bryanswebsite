import { HttpClient } from 'aurelia-fetch-client';
import { inject } from 'aurelia-framework';

@inject(HttpClient)
export class Photography {
    

    constructor(http) {
        this.httpObject = http;
        http.fetch('api/flickr/?method=flickr.photosets.getList&format=json&nojsoncallback=1')
            .then(result => result.json())
            .then(data => {
                this.data = data;
            });
    }

    getPhotos= function(psId)
    {
        this.httpObject.fetch('api/flickr/?method=flickr.photosets.getPhotos&format=json&nojsoncallback=1&photoset_id=' + psId)
            .then(result => result.json())
            .then(data => {
                this.photos = data;
            });
    }
 
}

