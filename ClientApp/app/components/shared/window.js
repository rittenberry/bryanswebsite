import {bindable} from 'aurelia-framework';
export class Window {
    @bindable header = "";
    @bindable class = "";
    zindex = 1;
    stylestring = ""
    constructor() {
        if (!!!window.windows) window.windows = [];
        this.updateZindex()
        window.windows.push(this);
    }
    onMouseDown = function(e)
    {
        this.updateZindex();
        return true;
    }
    updateZindex = function()
    {
        if (window.windows.length > 0)
        {
            var newzindex = window.windows.reduce((max, obj) => (max.zindex > obj.zindex) ? max : obj).zindex + 1;
            this.zindex = newzindex;
            this.stylestring = "z-index:" + this.zindex;
        }
    }
  
}