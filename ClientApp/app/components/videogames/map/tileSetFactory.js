import {Tile} from "./tile";
import {TileSet} from "./tileSet";

export class TileSetFactory {

    create(tileSetJson, width, height, selector)
    {
        var tileSet = new TileSet(width, height);
        for (var index = 0; index < tileSetJson.length; index++)
        {
            var tileInfo = tileSetJson[index];
            var tile = new Tile(tileInfo.name,tileInfo.terrainType, tileInfo.x, tileInfo.y, tileInfo.width, tileInfo.height);
            if (!!!selector)
            {
                tileSet[tileInfo.name] = tile;
            }
            else 
            {
                tileSet[selector(tileInfo)] = tile;
            }
        }
        return tileSet;
    }
}