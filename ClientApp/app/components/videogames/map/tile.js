import {Rectangle} from "../rectangle";

export class Tile {
    frame;
    terrainType;
    name;
    constructor(name, terrainType, x,y, width, height)
    {
        this.name = name;
        this.terrainType = terrainType;
        this.frame = new Rectangle(x, y, width, height);
    }
}