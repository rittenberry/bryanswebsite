import {Tile} from "./tile";

export class TileSet {
    tileWidth;
    tileHeight;
    constructor(tileWidth, tileHeight)
    {
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }
}