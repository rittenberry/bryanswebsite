import { Rectangle } from "./rectangle";

export class Camera {
    viewport;
    map;
    maxViewportHeight;
    maxViewportWidth;
    context;
    //remove later
    tileSet;
    //remove later
    tileSheet;
    constructor(context, map,viewport, tileSet, tileSheet)
    {
        this.tileSet = tileSet;
        this.tileSheet = tileSheet;
        this.context = context;
        this.map = map;
        this.viewport = viewport;
        this.maxViewportHeight = this.map.height - this.viewport.height;
        this.maxViewportWidth = this.map.width - this.viewport.width;
    }
    track(x,y, width, height)
    {

        this.viewport.x = Math.round((x + width/2) - this.viewport.width / 2);
        this.viewport.y = Math.round((y + height/2) - this.viewport.height / 2);

        if (this.viewport.x < 0)
        {
            this.viewport.x = 0;
        }
        else if (this.viewport.x > this.maxViewportWidth)
        {
            this.viewport.x =  this.maxViewportWidth;
        }
        if (this.viewport.y < 0)
        {
            this.viewport.y = 0;
        }
        else if (this.viewport.y >  this.maxViewportHeight)
        {
            this.viewport.y = this.maxViewportHeight;
        }
    }
    render(context)
    {
        if (!context){context = this.context;}
        
        for (var z = 0; z < this.map.layers.length; z++)
        {
            var currentLayer = this.map.layers[z];
            var minXIndex = Math.floor((this.viewport.x)/currentLayer.tileWidth);
            var maxXIndex = Math.floor((this.viewport.x+this.viewport.width)/currentLayer.tileWidth);
            var minYIndex = Math.floor((this.viewport.y)/currentLayer.tileHeight);
            var maxYIndex = Math.floor((this.viewport.y+this.viewport.height)/currentLayer.tileHeight);

            if (maxYIndex > currentLayer.tiles.length - 1)
            {
                maxYIndex = currentLayer.tiles.length - 1;
            }

             //for (var y = 0; y < this.map[z].length; y++)
             for(var y = minYIndex; y <= maxYIndex; y++)
             {
                if (maxXIndex > currentLayer.tiles[y].length - 1)
                {
                    maxXIndex = currentLayer.tiles[y].length - 1;
                }
                 //for (var x = 0; x < this.map[z][y].length; x++)
                 for(var x = minXIndex; x <= maxXIndex; x++)
                 {
                     var tile = currentLayer.tiles[y][x];
                     if (tile != -1)
                     {
                     var tileX = this.tileSet[tile].frame.x;
                     var tileY = this.tileSet[tile].frame.y;
                     context.drawImage(this.tileSheet, tileX, tileY, 32,32, x *32- this.viewport.x, y*32 - this.viewport.y,32,32);
                     }
                 }
             }
         }
    }

}