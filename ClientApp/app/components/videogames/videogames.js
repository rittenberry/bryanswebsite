// import { Animation } from './animation';
import {CharacterAnimationFactory} from './animation/factory/characterAnimationFactory';
import {TileSet} from './map/tileSet';
import {Tile} from './map/tile';
import {TileSetFactory} from './map/tileSetFactory';
import {Rectangle} from './rectangle';
import {Camera} from './camera';
import {tileJson} from './assets/tileSetConfig';
import {worldMap} from './assets/worldMapConfig';
export class Videogames {
    self = this;
    x = 30;
    y = 30;
    velocity = 200; // px/s
    movingPositiveX = 0;
    movingPositiveY = 0;
    movingNegativeX = 0;
    movingNegativeY = 0;
    background;
    backgroundAudio;
    coin;
    canvas;
    context;
    viewPort;
    map;    
    tileSheet;
    tiles;
    camera;
    animation;

    lastTimeStamp = 0;

    detached(argument) {
        self.backgroundAudio.pause();
        self.backgroundAudio.currentTime = 0;
     }

    Loop(timeStamp)
   {
       var elapsedTime = (timeStamp - self.lastTimeStamp)/1000;
       self.Process(elapsedTime, self);
       self.Draw(self);
       self.lastTimeStamp = timeStamp;
       window.requestAnimationFrame(function(t) {
        self.Loop(t)
    });
   }

    Process(elapsedTime)
   {
       var action = self.animation.currentAction;
       if (self.movingPositiveX + self.movingNegativeX === 0 && self.movingPositiveY + self.movingNegativeY === 0)
       {
            self.animation.idle();
       }
       if (self.movingPositiveY + self.movingNegativeY === -1)
       {
           action = 1;
       }
       if (self.movingPositiveY + self.movingNegativeY === 1)
       {
           action = 3;
       }

       if (self.movingPositiveX + self.movingNegativeX === -1)
       {
           action = 2;
       }
       if (self.movingPositiveX + self.movingNegativeX === 1)
       {
           action = 4;
       }

    var proposedx = self.x;
    var proposedy = self.y;
    proposedx += self.movingNegativeX * self.velocity * elapsedTime;
    proposedy += self.movingNegativeY * self.velocity * elapsedTime;
    proposedx += self.movingPositiveX * self.velocity * elapsedTime;
    proposedy += self.movingPositiveY * self.velocity * elapsedTime;

    var canPass = true;
    for (var layerIndex = 0; layerIndex < self.map.layers.length; layerIndex++)
    {
        var currentLayer = self.map.layers[layerIndex];
        var minXIndex = Math.floor((proposedx+16)/currentLayer.tileWidth);
        var maxXIndex = Math.floor((proposedx+48)/currentLayer.tileWidth);
        var minYIndex = Math.floor((proposedy+36)/currentLayer.tileHeight);
        var maxYIndex = Math.floor((proposedy+64)/currentLayer.tileHeight);

        if (minXIndex < 0) minXIndex = 0;
        if (maxXIndex < 0) maxXIndex = 0;
        if (minYIndex < 0) minYIndex = 0;
        if (maxYIndex < 0) maxYIndex = 0;

        if (minXIndex > 18) minXIndex = 18;
        if (maxXIndex > 18) maxXIndex = 18;
        if (minYIndex > 24) minYIndex = 24;
        if (maxYIndex > 24) maxYIndex = 24;
        

        for (var yIndex = minYIndex; yIndex <= maxYIndex; yIndex++)
        {
            for (var xIndex = minXIndex; xIndex <= maxXIndex; xIndex++)
            {
 
                var tileIndex = currentLayer.tiles[yIndex][xIndex];
                if (tileIndex != -1)
                {
                    var currentTile = self.tileSet[tileIndex];
                    if (currentTile != undefined)
                    {
                    if (currentTile.terrainType == 2)
                    {
                        canPass =false;
                        break;
                    }
                    }
                }
            }
            if (!canPass)
            {
                break;
            }
        }
        if (!canPass)
        {
            break;
        }
    }
    if (canPass)
    {
        self.x =proposedx;
        self.y = proposedy;
        self.animation.animate(action, elapsedTime);
        self.camera.track(self.x, self.y, self.animation.width, self.animation.height);
    }

   }
    Draw()
   {
       if (!self.context)
       {
        self.canvas = document.getElementById("myCanvas");
        self.context = self.canvas.getContext("2d");
    
        }

        self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);

       self.camera.render(self.context);
       
       var frame = self.animation.getCurrentAnimationFrame();
       self.context.drawImage(
        self.animation.spriteImage,
        frame.x,
        frame.y,
        frame.width,
        frame.height,
        self.x - self.camera.viewport.x,
        self.y - self.camera.viewport.y,
        self.animation.width,
        self.animation.height);
        
   }

    keyDown(args)
   {

       if (args.key == 'ArrowUp' || args.key == 'w')
       self.movingNegativeY = -1
       if (args.key == 'ArrowDown' || args.key == 's')
       self.movingPositiveY = 1;
       if (args.key == 'ArrowLeft' || args.key == 'a')
       self.movingNegativeX = -1;
       if (args.key == 'ArrowRight' || args.key == 'd')
       self.movingPositiveX = 1;

   }

    keyUp(args)
   {
       if (args.key == 'ArrowUp' || args.key == 'w')
       self.movingNegativeY = 0
       if (args.key == 'ArrowDown' || args.key == 's')
       self.movingPositiveY = 0;
       if (args.key == 'ArrowLeft' || args.key == 'a')
       self.movingNegativeX = 0;
       if (args.key == 'ArrowRight' || args.key == 'd')
       self.movingPositiveX = 0;

   }


constructor()
{
    self = this;
    this.init();
}
init()
{
    addEventListener("keydown", function(args) {self.keyDown(args)})
    addEventListener("keyup", function(args) {self.keyUp(args)})

    var canvas = document.getElementById("myCanvas");
    self.viewport = new Rectangle(0,0, 400, 400);

    self.coin = new Image();
    self.coin.src = "images/character.png";

    var factory = new CharacterAnimationFactory();
    
       self.animation = factory.create(self.coin, 9, 64,64, 0,64*8,64,64,8,0);
    

   window.requestAnimationFrame(function(timeStamp) {
    self.Loop(timeStamp)
   });

   self.tileSheet = new Image();
   self.tileSheet.src = "images/map.png";

   var tileSetFactory = new TileSetFactory();
   
   self.tileSet = tileSetFactory.create(tileJson, 32,32, function(e) {return e.name});

   self.map = worldMap;

   self.camera = new Camera(null, self.map, self.viewport, self.tileSet, self.tileSheet);

   self.background = new Image();
   self.background.src = "images/background.png";
   self.backgroundAudio = new Audio();
//    self.backgroundAudio.src = "audio/background.mp3";
//    self.backgroundAudio.loop = true;
//    self.backgroundAudio.play();
}
}