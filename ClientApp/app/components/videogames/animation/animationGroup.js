import {AnimationFrame} from "./animationFrame";

export class AnimationGroup {

    animationFrames;
    currentFrame;
    idleFrame;
    elapsedTime = 0;
    frameWidth;
    frameHeight;
    defaultDuration;

    constructor(animationSpeed)
    {   
        this.animationFrames = [];
        this.defaultDuration = 1/animationSpeed;
    }
    

    getFrameCount()
    {
        return this.animationFrames.length;
    }

    animate(elapsedTime)
    {
        this.elapsedTime += elapsedTime;
        if (this.currentFrame.duration == null)
        {
            if (this.elapsedTime > this.defaultDuration)
            {
                console.log("progress frame");
                this.progressAnimation();
            }
        }
        else
        {
            if (this.elapsedTime > this.currentFrame.duration)
            {
                this.progressAnimation();
            }
        }
    }

    resetAnimation()
    {
        this.elapsedTime = 0;
    }

    progressAnimation()
    {
        var currentFrameIndex;
        currentFrameIndex = this.animationFrames.indexOf(this.currentFrame);
        currentFrameIndex++;
        if (currentFrameIndex >= this.getFrameCount())
        {
            currentFrameIndex = 0;
        }
        this.currentFrame = this.animationFrames[currentFrameIndex];
        this.elapsedTime = 0;
    }
    
    idle()
    {
        this.currentFrame = this.idleFrame;
        this.elapsedTime = 0;
    }
}