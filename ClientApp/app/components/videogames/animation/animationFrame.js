import {Rectangle} from "../rectangle";

export class AnimationFrame {
    frame;
    duration;

    constructor(x,y, width, height)
    {
        this.frame = new Rectangle(x, y, width, height);
    }
}