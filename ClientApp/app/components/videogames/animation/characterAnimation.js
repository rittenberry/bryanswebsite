import {AnimationGroup} from "./animationGroup";
import { AnimationBase } from "./animationBase";

export class CharacterAnimation extends AnimationBase {
    

    constructor(image, width, height)
    {
        super();
        this.spriteImage = image;
        this.width = width;
        this.height = height;
    }

    spriteImage;
    animationGroups;
    currentAction;
    currentAnimationGroup;

    getCurrentAnimationFrame()
    {
        return this.currentAnimationGroup.currentFrame.frame;
    }

    animate(action, elapsedTime)
    {
        if (action != this.currentAction)
        {
            this.currentAnimationGroup.resetAnimation();
            this.currentAction = action;
            this.currentAnimationGroup = this.animationGroups[this.currentAction];
        }

        this.currentAnimationGroup.animate(elapsedTime);
    }

    idle()
    {
        this.currentAnimationGroup.idle();
    }
}
