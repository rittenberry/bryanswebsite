import {AnimationGroupFactory} from "./animationGroupFactory";
import {CharacterAnimation} from "../characterAnimation";
export class CharacterAnimationFactory {
    
    create (image, numberOfFrames, displayWidth, displayHeight, initFrameX, initFrameY, spriteWidth, spriteHeight, animationSpeed, idleFrameIndex) {
        var ca = new CharacterAnimation(image, displayWidth, displayHeight);
        var animationGroupFactory = new AnimationGroupFactory();
        ca.animationGroups = {};
        ca.animationGroups[1] =  animationGroupFactory.create(initFrameX,initFrameY, spriteWidth, spriteHeight, numberOfFrames, idleFrameIndex, animationSpeed);
        ca.animationGroups[2] =  animationGroupFactory.create(initFrameX,initFrameY + spriteHeight * 1, spriteWidth, spriteHeight, numberOfFrames, idleFrameIndex, animationSpeed);
        ca.animationGroups[3] =  animationGroupFactory.create(initFrameX,initFrameY + spriteHeight * 2, spriteWidth, spriteHeight, numberOfFrames, idleFrameIndex, animationSpeed);
        ca.animationGroups[4] =  animationGroupFactory.create(initFrameX,initFrameY + spriteHeight * 3, spriteWidth, spriteHeight, numberOfFrames, idleFrameIndex, animationSpeed);
        //ca.AnimationGroups.Add(ActionType.MoveRight, AnimationGroupFactory.Create(0, Constants.DefaultFrameHeight, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));
        //ca.AnimationGroups.Add(ActionType.MoveDown, AnimationGroupFactory.Create(0, Constants.DefaultFrameHeight * 2, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));
        //ca.AnimationGroups.Add(ActionType.MoveLeft, AnimationGroupFactory.Create(0, Constants.DefaultFrameHeight * 3, Constants.DefaultFrameWidth, Constants.DefaultFrameHeight, 3, 1));

        ca.currentAnimationGroup = ca.animationGroups[1];
        ca.framesPerSecond = animationSpeed;
        
        return ca;
    }
}
