import {AnimationGroup} from "../animationGroup";
import {AnimationFrame} from "../animationFrame";
export class AnimationGroupFactory {

    create(x, y, frameWidth, frameHeight, numberOfFrames, idleFrame, animationSpeed)
    {
        var animationGroup = new AnimationGroup(animationSpeed);

        for (var i = 0; i < numberOfFrames; i++)
        {
            var frame = new AnimationFrame(x + i * frameWidth, y, frameHeight, frameWidth);
            animationGroup.animationFrames.push(frame);
            if (i == idleFrame)
            {
                animationGroup.idleFrame = frame;
            }
        }

        animationGroup.currentFrame = animationGroup.idleFrame;

        return animationGroup;
    }
}