const ActionTypes = Object.freeze({
    MoveUp: 1,
    MoveRight: 2,
    MoveDown: 3,
    MoveLeft: 4,
    Idle: 5,
    Action1: 6,
    Action2: 7
});
