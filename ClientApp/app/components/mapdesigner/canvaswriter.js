export class CanvasWriter {
    canvas;
    methods;
    obj;
    // canvas: is the canvas you wish to write to.
    // obj: any object context you wish to pass to any function when writing.
    constructor(canvas, obj) {
        this.canvas = canvas;
        this.methods = [];
        this.obj = obj;
    }
    // You can queue any methods to draw to the canvas with the parameters of the canvas context, canvas, and an object context
    Queue = function(method)
    {
        this.methods.push(method)
    }
    // Executes all of the queued methods.
    Write = function()
    {
        var canvasContext = this.canvas.getContext('2d');
        this.methods.forEach(method => {
            method(canvasContext, this.canvas, this.obj);
        });
    }
    // Executes method passed in
    WriteMethods = function(methods)
    {
        var canvasContext = this.canvas.getContext('2d');
        if (Array.isArray(methods))
        {
            methods.forEach(method => {
                method(canvasContext, this.canvas, this.obj);
            });
        }
        else if (!!methods)
        {
            methods(canvasContext, this.canvas, this.obj);
        }
    }
}