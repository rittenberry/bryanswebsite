import {CanvasWriter} from './canvaswriter';

export class MapDesigner {
    tileSets = [];
    map;
    currentTile;
    layers;
    newLayerWidth = 32;
    newLayerHeight = 32;
    selectedLayer = null;
    showAllLayers = false;
    constructor() {
        this.map = { test: "foo", width:0, height:0, layers: [], tileSets: this.tileSets };
        this.layers = this.map.layers;
    }

    getTileSetCanvas = function(obj)
    {
        if (!!!obj.canvas)
        {
            obj.canvas = document.getElementById('myCanvas');
            obj.canvas.addEventListener('click', function(event) { obj.tileSetCanvasClick(event, obj) }, false);
        }
        return obj.canvas;   
    }
    getMapCanvas = function(obj)
    {
        if (!!!obj.mapCanvas)
        {
            obj.mapCanvas = document.getElementById('myMapCanvas');
            obj.mapCanvas.addEventListener('click', function(event) { obj.mapCanvasClick(event, obj) }, false);
            obj.mapCanvas.addEventListener('mousemove', function(event) { obj.mapCanvasMouseMove(event, obj) }, false)
        }
        return obj.mapCanvas;   
    }
    clearCanvas = function(context, canvas)
    {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    drawMap = function(context, obj)
    {
        var image = obj.image
        var currentLayer = obj.selectedLayer;


        obj.layers.forEach(layer => {
            if (this.showAllLayers || layer.show)
            {
                var tile_height = currentLayer.tileHeight;
                var tile_width = currentLayer.tileWidth;
                var tilesX = currentLayer.tilesX;
                var tilesY = currentLayer.tilesY;
                for (var y = 0; y < tilesY; y++)
                {
                    for (var x = 0; x < tilesX; x++)
                    {
                        var tileIndex = layer.tiles[y][x];
                        if(tileIndex != -1)
                        {
                            var tile = obj.tileSets[0].tiles[tileIndex];
                            context.drawImage(image,
                                tile.x,
                                tile.y,
                                tile.width,
                                tile.height,
                                x * tile_width,
                                y * tile_height,
                                tile_width,
                                tile_height);
                        }
                    }
                }
            }
        });
    }

    drawHoverOver = function(context, x, y, width, height, fill)
    {
        context.fillStyle = fill;
        context.globalCompositeOperation = "multiply";
        context.fillRect(x ,y ,width, height);
        context.globalCompositeOperation = "source-over";
    }
    getMouseCanvasPosition = function(canvas, event)
    {
        var rect = canvas.getBoundingClientRect()
        var x = event.clientX - rect.left
        var y = event.clientY - rect.top
        return {x: x, y: y};
    }
    mapCanvasMouseMove = function(event, obj) 
    {
        var writer = new CanvasWriter(obj.mapCanvas);
        var {x, y} = this.getMouseCanvasPosition(obj.mapCanvas, event);
        
        var currentLayer = obj.selectedLayer
        var tile_height = currentLayer.tileHeight;
        var tile_width = currentLayer.tileWidth;

        var tile_y =  Math.floor(y / (tile_height))
        var tile_x = Math.floor(x / (tile_width))

        if (event.buttons == 1)
        {
            var currentLayer = obj.selectedLayer
            if (!!obj.currentTile)
            {
                currentLayer.tiles[tile_y][tile_x] = obj.currentTile.id;
            }
        }

        var position_x = tile_x * tile_width;
        var position_y = tile_y * tile_height;

        var writer = new CanvasWriter(obj.mapCanvas);
        writer.Queue(this.clearCanvas)
        writer.Queue(((obj) => (context) => this.drawMap(context, obj))(this))
        writer.Queue(((position_x ,position_y ,tile_width , tile_height) => (context) => this.drawHoverOver(context, position_x ,position_y ,tile_width , tile_height, "#0ff"))(position_x ,position_y ,tile_width , tile_height));
        writer.Write();
    }
    mapCanvasClick = function(event,obj)
    {
        var writer = new CanvasWriter(obj.mapCanvas);
        var {x, y} = this.getMouseCanvasPosition(obj.mapCanvas, event);
        console.log("x: " + x + " y: " + y)

        var currentLayer = obj.selectedLayer
        var tile_height = currentLayer.tileHeight;
        var tile_width = currentLayer.tileWidth;

        var tile_y =  Math.floor(y / tile_height)
        var tile_x = Math.floor(x / tile_width)

        if (!!obj.currentTile)
        {
            currentLayer.tiles[tile_y][tile_x] = obj.currentTile.id;
        }

        writer.Queue(this.clearCanvas)
        writer.Queue(((obj) => (context) => this.drawMap(context, obj))(this))
        writer.Write();
    }
    tileSetCanvasClick = function(event, obj)
    {
        const rect = obj.canvas.getBoundingClientRect()
        var x = event.clientX - rect.left
        var y = event.clientY - rect.top
        var tile_height = obj.newLayerHeight;
        var tile_width = obj.newLayerWidth;

        var tile_y =  Math.floor(y / (tile_height + 2))
        var tile_x = Math.floor(x / (tile_width + 2))

        var tiles_wide = obj.image.width / tile_width;

        var index = tiles_wide * tile_y + tile_x;
        obj.currentTile = obj.tileSets[0].tiles[index];
        obj.currentTile = obj.currentTile;



        obj.drawTileSet(obj);

        var ctx = obj.canvas.getContext('2d');
        ctx.fillStyle = "#0ff";
        ctx.globalCompositeOperation = "multiply";

        var position_x = tile_x * tile_width + 2 * (tile_x + 1) -1;
        var position_y = tile_y * tile_height + 2 * (tile_y + 1) -1;
        ctx.fillRect(position_x ,position_y ,tile_width + 2, tile_height + 2);
        console.log("x: " + x + " y: " + y)
        console.log("tile_x: " + tile_x + "tile_y: " + tile_y);
        console.log("position_x: " + position_x + " position_y: " + position_y)
    }

    getIntValueFromElement = function(elementName, obj)
    {
        if (!!!obj) { obj = this;}
        return parseInt(document.getElementById(elementName).value);
    }

    downloadMap = function()
    {
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.map));
        var downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href",     dataStr);
        downloadAnchorNode.setAttribute("download", "map" + ".json");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    }
    importMap = function()
    {
        var file = document.getElementById('file-selector').files[0];
        var fr=new FileReader();
        fr.onload=function() {
            this.map = JSON.parse(fr.result);
            alert(JSON.stringify(this.map));
        }
        fr.readAsText(file);



    }
    drawTileSet = function(obj, image)
    {
        var canvas = obj.getTileSetCanvas(obj)
        if (!!!image)
        {
            image = obj.image
        }
        var tile_height = obj.newLayerHeight;
        var tile_width = obj.newLayerWidth;
        var tiles_wide = image.width / tile_width;
        var tiles_tall = image.height / tile_height;
                
        canvas.width = image.width + 2*tiles_wide;
        canvas.height = image.height + 2 * tiles_tall;
        var ctx = canvas.getContext('2d');
        
        var index = 0
        var tile_set = obj.tileSets[0];
        for (var y = 0; y < tiles_tall; y++)
        {
            for (var x = 0; x < tiles_wide; x++)
            {
                var tile = tile_set.tiles[index];
                ctx.drawImage(image,
                tile.x,
                tile.y,
                tile.width,
                tile.height,
                x * tile_width + 2 * (x + 1),
                y * tile_height + 2 * (y + 1),
                tile_width,
                tile_height);
                index++;
            }
        }
    }

    importImage = function(obj)
    {
        var obj = this;
        obj.foo = 8;
        var file = document.getElementById('image-selector').files[0];
        var fr=new FileReader();
        fr.onload=function() {
            var image = new Image();
            image.onload = function(ev) {
                obj.image = image;
                var tile_height = obj.newLayerHeight;
                var tile_width = obj.newLayerWidth;
                var tiles_wide = image.width / tile_width;
                var tiles_tall = image.height / tile_height;
                var tile_set = { tiles: []};
                var index = 0;

                for (var y = 0; y < tiles_tall; y++)
                {
                    for (var x = 0; x < tiles_wide; x++)
                    {
                        tile_set.tiles[index] = {id:index, x: x * tile_width, y: y * tile_height, width: tile_width, height: tile_height, terrainType: 1, name: "img" + index};
                        index++;
                    }
                }
                obj.tileSets[0] = tile_set;
                obj.foo = 10;
                obj.drawTileSet(obj, image);
            }
            image.src = fr.result;

        }
        fr.readAsDataURL(file);

        var tilesX = this.getIntValueFromElement("tilesX");
        var tilesY = this.getIntValueFromElement("tilesY");
        var mapCanvas = this.getMapCanvas(obj);

        var tile_height = this.newLayerHeight;
        var tile_width = this.newLayerWidth;

        mapCanvas.width = tilesX * tile_width;
        mapCanvas.height =tilesY * tile_height;
        this.map.width = tilesX * tile_width;
        this.map.height = tilesY * tile_height;

        this.addNewLayer("default", this.newLayerWidth, this.newLayerHeight)
    }

    fillLayer = function(layer, tile)
    {
        for (var x = 0; x < layer.tilesX; x++)
        {
            layer.tiles[x].fill(tile.id, 0, layer.tilesY);
        }
        this.redrawMap(this);
    }

    createMap = function(width, height, name)
    {
        this.map = { name: name, width: width, height: height, layers: [] };
    }


    updateMap = function()
    {

    }

    setEraseTile = function()
    {
        this.currentTile = { id: -1 }; 
    }
    
    createTileSet = function()
    {

    }
    removeTileSet = function(tileSet)
    {
        var index = this.tileSets.indexOf(tileSet);
        if (index > -1) this.tileSets.splice(index, 1);
    }
    createLayer = function(name, tileWidth, tileHeight)
    {
        //var existingLayer = this.map.layers.find(element => element.name == name);
        //var index;
        //if (existingLayer == null) index = this.map.layers.push( {  } ) -1;
        var layer = { name: name, tileWidth: tileWidth, tileHeight:tileHeight, show: true}

        if (this.layers.length > 0)
        {
            layer.id = this.layers.reduce((max, obj) => (max.id > obj.id) ? max : obj).id + 1;
        }
        else
        {
            layer.id = 0;
        }

        var tilesX = this.getIntValueFromElement("tilesX");
        var tilesY = this.getIntValueFromElement("tilesY");

        layer.tilesX = tilesX;
        layer.tilesY = tilesY;

        layer.tiles = Array(tilesX).fill().map(() => Array(tilesY).fill(-1));
 
        return layer;
    }
    addLayer = function(layer)
    {
        this.layers.push(layer);
        this.selectedLayer = layer;
    }
    addNewLayer = function(name, tileWidth, tileHeight)
    {
        var layer = this.createLayer(name, tileWidth, tileHeight)
        this.addLayer(layer);
        this.redrawMap(this);
        this.newLayerName = null;
    }



    removeLayer = function(layer)
    {
        if (layer.name != 'default')
        {
            var index = this.layers.indexOf(layer);
            if (index > -1)
            {
                this.layers.splice(index, 1);
                this.selectedLayer = this.layers[index -1]
                this.redrawMap(this);
            }
        }
    }
    redrawMap = function (obj)
    {
        if (!!!obj) { obj = this; }
        var writer = new CanvasWriter(this.mapCanvas);
        writer.Queue(this.clearCanvas)
        writer.Queue(((obj) => (context) => this.drawMap(context, obj))(obj))
        writer.Write();
    }
    changeSelectedLayer = function(layer)
    {
        this.selectedLayer = layer;
        this.redrawMap(this);
    }
    changeLayerVisibility = function(layer)
    {
        layer.show = !layer.show;
        this.redrawMap(this);
    }

}

