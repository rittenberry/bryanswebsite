import { Aurelia, PLATFORM } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

export class App {
    configureRouter(config, router) {
        config.title = 'Aurelia';
        config.map([{
            route: [ '', 'home' ],
            name: 'home',
            settings: { icon: 'home' },
            moduleId: PLATFORM.moduleName('../home/home'),
            nav: true,
            title: 'Home'
        }, {
            route: 'photography',
            name: 'photography',
            settings: { icon: 'camera' },
            moduleId: PLATFORM.moduleName('../photography/photography'),
            nav: true,
            title: 'Photography'
        }, {
            route: 'programming',
            name: 'programming',
            settings: { icon: 'code' },
            moduleId: PLATFORM.moduleName('../programming/programming'),
            nav: true,
            title: 'Programming'
        }, {
            route: 'music',
            name: 'music',
            settings: { icon: 'music' },
            moduleId: PLATFORM.moduleName('../music/music'),
            nav: true,
            title: 'Music'
        }, {
            route: 'videogames',
            name: 'videogames',
            settings: { icon: 'gamepad' },
            moduleId: PLATFORM.moduleName('../videogames/videogames'),
            nav: true,
            title: 'Video Games'
        }, {
            route: 'mapdesigner',
            name: 'mapdesigner',
            settings: { icon: 'gamepad' },
            moduleId: PLATFORM.moduleName('../mapdesigner/mapdesigner'),
            nav: true,
            title: 'Map Designer'
        }
        
        , {
            route: 'contacts',
            name: 'contacts',
            settings: { icon: 'address-card' },
            moduleId: PLATFORM.moduleName('../contacts/contacts'),
            nav: true,
            title: 'Contact Information'
        }
        
        ]);

        this.router = router;
    }
}
