FROM mcr.microsoft.com/dotnet/runtime:6.0-alpine
WORKDIR /app
ENV ASPNETCORE_URLS http://*:5000
EXPOSE 5000
COPY . /app
ENTRYPOINT ["dotnet", "bryanswebsite.dll"]
