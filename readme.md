
Data Definitions

map
{
    name: "the name of the map",
    width: "pixels wide",
    height: "pixels high",
    layers: [] "array of layers that the map is made of" 
}

layer
{
    name: "the name of the layer",
    show: "boolean of whether a layer is visible",
    collisionDetection: "boolean that can be used ti turn on or off collision detection for a layer",
    tileWidth: "width of a tile to draw",
    tileHeight: "height of a tile to draw",
    tilesX: "number of tiles wide",
    tilesY: "number of tiles tall"
    tiles: [] "two dimensional array of tiles" 
}

tileSet
{
    name: "the name of the tile set",
    id: "the id of the tile set",
    tilesheet: "the tilesheet image",
    tiles: [] "the tiles that point to locations on the tilesheet"
}

tile
{
    name: "the name of the tile",
    id: "the id of the tile", 
    x: "the starting x position of the tile in the tilesheet", 
    y: "the starting y position of the tile in the tilesheet",
    width: "the width of the tile in the tilesheet", 
    height: "the height of the tile in the tilesheet", 
    terrainType: "the type of the terrain used for collision detection to determine what can move into the layer"
}